<%--
  Created by IntelliJ IDEA.
  User: Creed
  Date: 2020/9/14
  Time: 13:53
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!--
START: Navbar

Additional Classes:
.dx-navbar-sticky || .dx-navbar-fixed
.dx-navbar-autohide
.dx-navbar-dropdown-triangle
.dx-navbar-dropdown-dark - only <ul>
.dx-navbar-expand || .dx-navbar-expand-lg || .dx-navbar-expand-xl
-->
<nav class="dx-navbar dx-navbar-top dx-navbar-collapse dx-navbar-sticky dx-navbar-expand-lg dx-navbar-dropdown-triangle dx-navbar-autohide">
    <div class="container">

        <a href="../" class="dx-nav-logo">
            <img src="../assets/images/logo.png" alt="" width="88px">
        </a>

        <button class="dx-navbar-burger">
            <span></span><span></span><span></span>
        </button>

        <div class="dx-navbar-content">
            <ul class="dx-nav dx-nav-align-left">
                <li class="dx-drop-item ${param.menu=='index'?'active':''}">
                    <a href="../" name="index">
                        首页
                    </a>
                </li>
                <li class="dx-drop-item ${param.menu=='search'?'active':''}">
                    <a href="../adsearch/adsearch" name="search">
                        搜索
                    </a>
                </li>
                <li class="dx-drop-item ${param.menu=='label'?'active':''}">
                    <a href="../labels/labels?id=" name="label">
                        分类
                    </a>
                </li>
                <li class="dx-drop-item ${param.menu=='group'?'active':''}">
                    <a href="<%=request.getContextPath()%>/group/group" name="group">
                        群组
                    </a>
                </li>
                <li class="dx-drop-item ${param.menu=='newres'?'active':''}">
                    <a href="../crudres/new" name="newres">
                        新建
                    </a>
                </li>
            </ul>

            <ul class="dx-nav dx-nav-align-right">
                        <li class="dx-drop-item">
                            <a href="#">登录</a>
                            <ul class="dx-navbar-dropdown">
                                <li>
                                    <a id="psw-reset-button1" href="#">修改密码</a>
                                </li>
                                <li>
                                    <a id="personal-data-button1" href="#">个人信息</a>
                                </li>
                                <li>
                                    <a id="myresource-data-button1" href="#">我的</a>
                                </li>
                                <li>
                                    <a id="log-out-button1" href="#" onclick="logout()">退出</a>
                                </li>
                            </ul>
                        </li>
            </ul>
        </div>
    </div>
</nav>
<!-- END: Navbar -->

