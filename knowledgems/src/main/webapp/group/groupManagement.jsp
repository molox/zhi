<%--
  Created by IntelliJ IDEA.
  User: Creed
  Date: 2021/1/8
  Time: 0:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <c:import url="/template/_header.jsp"/>
    <title>群组管理-知了[团队知识管理应用]</title>
</head>

<body>

<div class="re-main">
    <c:import url="/template/_navbar.jsp"/>
    <div class="re-box-6 re-grey mt-85">
        <div class="container">
            <div class="row vertical-gap md-gap">
                <div class="col-lg-12">
                    <div class="re-box re-box-decorated">
                        <div class="resource-post">
                            <div class="resource-post-box pt-30 pb-25">

                                <ul class="re-breadcrumbs pb-25">
                                    <a href="#" onclick="javascript :history.back(-1);">管理</a>&nbsp;|&nbsp;群组管理
                                </ul>

                                <div class="resource-comment-text">
                                    <h1 class="h4 mnt-5 mb-9"><span id="groupName">群组管理</span></h1>
                                </div>

                                <div class="container" id="loading-container">
                                    <div class="front-loading">
                                        <img src="../assets/images/loading.gif"/>
                                    </div>
                                    <div class="panel-body text-center">正在加载请稍候</div>
                                </div>

                                <div id="groupList"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="addGroup" tabindex="-1" role="dialog" aria-labelledby="addMemberLabel" data>
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel">新建群组</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>群组名称</label>
                            <div class="col-lg-7">
                                <input class="form-control" id="newName" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label>群主姓名</label>
                            <div class="col-lg-7">
                                <select class="selectpicker form-control" id="leaderName" name="leaderName" data-live-search="true"></select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <%--                    <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span>关闭</button>--%>
                        <button type="button" id="btn_submit" class="btn btn-primary" data-dismiss="modal"
                                onclick="addGroup()"><span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span>新增</button>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <c:import url="../template/_footer.jsp"/>
</div>

</body>

<!-- START: Scripts -->
<c:import url="../template/_include_js.jsp"/>
<script>
    var searchValue = "";
    showGroups(1,'');

    document.addEventListener("keyup",function(e){
        if(e.keyCode == 13){
            search();
        }
    });

    function search(){
        var inputValue = $('#searchBox').val();
        searchValue = inputValue;
        showGroups(1,inputValue);
    }

    function getAppPage(page){
        showGroups(page, searchValue);
    }

    function showGroups(page, search) {
        $.ajax({
            url: "/groupManagement",
            type: "get",
            data: {page:page, search:search},
            beforeSend:function(){
                $("#loading-container").css("display","block");
                $("#groupList").hide();
            },
            success: function (data) {
                $('#groupList').html(data);
                //获取查到的群组数量
                var parser = new DOMParser();
                var htmlDoc = parser.parseFromString("" + data, "text/html");
                // var searchedNum = htmlDoc.getElementById("searchedGroupNum").value;
                // if(search.trim() === "")
                //     $("#searchedText").text("当前共有" + searchedNum + "个群组");
                // else
                //     $("#searchedText").text("当前共有" + searchedNum + "个群组含\""+search+"\"关键字");
                // if(search.trim() === "")
                //     $("#searchedText").text("当前共有个群组");
                // else
                //     $("#searchedText").text("当前共有个群组含\""+search+"\"关键字");
                $("#loading-container").css("display","none");
                $("#groupList").show();
            },
            error: function (data) {
                $.fillTipBox({type: 'warning', icon: 'glyphicon-exclamation-sign', content: '查询群组失败！'});
            }
        });
    }

    function deleteGroup(groupId){
        $.ajax({
            url:"/deleteGroup",
            type:"post",
            async: false,
            dataType:"text",
            data:{groupId:groupId,
            },
            success:function(data) {
                if(data=="ok") {
                    $.fillTipBox({type: 'success', icon: 'glyphicon-ok-sign', content: '删除群组成功'});
                    getMember(${groupId});
                }else{
                    // $.fillTipBox({type: 'error', icon: 'glyphicon-alert', content: '设置管理员失败'});
                }
            },
        });
    }

    function confirmDelete(id) {
        $.tipModal('confirm', '','删除此群组将导致资源从本群组中移除，而不会删除相关资源。请确认删除该群组吗？', function (result) {
            if (result == true) {
                deleteGroup(id);
                search();
            }
        });
    }

</script>
<!-- END: Scripts -->

</html>
