<%--
  Created by IntelliJ IDEA.
  User: liyunze
  Date: 2020/11/25
  Time: 下午2:24
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html lang="zh-Hans">
<head>
    <c:import url="../template/_header.jsp"/>
    <link rel="stylesheet" type="text/css">
    <script src="../assets/js/labels.js"></script>
    <title>分类管理-知了[团队知识管理应用]</title>
</head>
<body>
<div class="re-main">
    <c:import url="../template/_navbar.jsp"/>
    <div class="re-box-6 re-grey mt-85">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-12 col-xl-12">
                    <div class="re-box-decorated">
                        <div class="re-box-content" style="padding-left: 0">
                            <div class="re-form-group"  style="line-height:38px; margin-top: -5px; margin-bottom: 15px; padding-left: 48px;">
                                <a href="../manage">管理</a><a> | 分类管理</a>
                            </div>
                            <div class="re-comment-text" style="margin-left: 48px; margin-bottom: 17px">
                                <h1 class="h4 mnt-5 mb-9"><span id="userName">分类管理</span></h1>
                            </div>
                            <div id="labels" class="re-form-group" style="line-height:38px; margin-top: 1px"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <c:import url="../template/_footer.jsp"/>
</div>

<div class="modal fade" id="addMember" tabindex="-1" role="dialog" aria-labelledby="addMemberLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">新增一级类别</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="form-group" style="display: none">
                    <div class="col-lg-7">
                        <label>上级ID</label>
                        <input type="text" class="form-control front-no-box-shadow" name="applicationDes" id="label_uplevelId" readonly/>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-lg-7">
                        <label>类别名称</label>
                        <input type="text" class="form-control front-no-box-shadow" name="applicationDes" id="label_name" placeholder="必填"/>
                    </div>
                </div>
                <div class="form-group" style="display: none">
                    <div class="col-lg-7">
                        <label>标签描述</label>
                        <input type="text" class="form-control front-no-box-shadow" name="applicationDes" id="label_info" placeholder="必填"/>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="btn_submit" class="btn btn-primary" data-dismiss="modal"
                        onclick="addLabel()">新增</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="delMember" tabindex="-1" role="dialog" aria-labelledby="addMemberLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="form-group">
                    <div class="col-lg-7">
                        <label>确定删除一级类别？</label>
                        <input type="text" class="form-control front-no-box-shadow" name="applicationDes" id="tagId" style="display:none;"/>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="cancel" class="btn btn-default" data-dismiss="modal"
                        onclick="hideDelDialog()">取消</button>
                <button type="button" id="confirm" class="btn btn-primary" data-dismiss="modal"
                        onclick="delLabel()"><a>确定</a></button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="updateMember" tabindex="-1" role="dialog" aria-labelledby="addMemberLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">修改一级类别</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="form-group" style="display: none">
                    <div class="col-lg-7">
                        <label>上级ID</label>
                        <input type="text" class="form-control front-no-box-shadow" name="applicationDes" id="label_uplevelId1" readonly/>
                    </div>
                </div>
                <div class="form-group" style="display: none">
                    <div class="col-lg-7">
                        <label>标签ID</label>
                        <input type="text" class="form-control front-no-box-shadow" name="applicationDes" id="tagId1" readonly/>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-lg-7">
                        <label>类别名称</label>
                        <input type="text" class="form-control front-no-box-shadow" name="applicationDes" id="label_name1"/>
                    </div>
                </div>
                <div class="form-group" style="display: none">
                    <div class="col-lg-7">
                        <label>标签描述</label>
                        <input type="text" class="form-control front-no-box-shadow" name="applicationDes" id="label_info1"/>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="btn_submit1" class="btn btn-primary" data-dismiss="modal"
                        onclick="updateLabel()"><a>确定</a></button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="addMemberForTwo" tabindex="-1" role="dialog" aria-labelledby="addMemberLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabelForTwo">新增二级类别</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="form-group" style="display: none">
                    <div class="col-lg-7">
                        <label>上级ID</label>
                        <input type="text" class="form-control front-no-box-shadow" name="applicationDes" id="label_uplevelIdForTwo" readonly/>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-lg-7">
                        <label>一级类别</label>
                        <input type="text" class="form-control front-no-box-shadow" name="applicationDes" id="label_uplevelNameForTwo" readonly/>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-lg-7">
                        <label>类别名称</label>
                        <input type="text" class="form-control front-no-box-shadow" name="applicationDes" id="label_nameForTwo" placeholder="必填"/>
                    </div>
                </div>
                <div class="form-group" style="display: none">
                    <div class="col-lg-7">
                        <label>标签描述</label>
                        <input type="text" class="form-control front-no-box-shadow" name="applicationDes" id="label_infoForTwo" placeholder="必填"/>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="btn_submitForTwo" class="btn btn-primary" data-dismiss="modal"
                        onclick="addLabelForTwo()">新增</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="delMemberForTwo" tabindex="-1" role="dialog" aria-labelledby="addMemberLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="form-group">
                    <div class="col-lg-7">
                        <label>确定删除二级类别？</label>
                        <input type="text" class="form-control front-no-box-shadow" name="applicationDes" id="tagIdForTwo" style="display:none;"/>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="cancelForTwo" class="btn btn-default" data-dismiss="modal"
                        onclick="hideDelDialogForTwo()">取消</button>
                <button type="button" id="confirmForTwo" class="btn btn-primary" data-dismiss="modal"
                        onclick="delLabelForTwo()"><a>确定</a></button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="updateMemberForTwo" tabindex="-1" role="dialog" aria-labelledby="addMemberLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1ForTwo">修改二级类别</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="form-group" style="display: none">
                    <div class="col-lg-7">
                        <label>上级ID</label>
                        <input type="text" class="form-control front-no-box-shadow" name="applicationDes" id="label_uplevelId1ForTwo" readonly/>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-lg-7">
                        <label>一级类别</label>
                        <input type="text" class="form-control front-no-box-shadow" name="applicationDes" id="label_uplevelName1ForTwo" readonly/>
                    </div>
                </div>
                <div class="form-group" style="display: none">
                    <div class="col-lg-7">
                        <label>标签ID</label>
                        <input type="text" class="form-control front-no-box-shadow" name="applicationDes" id="tagId1ForTwo" readonly/>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-lg-7">
                        <label>类别名称</label>
                        <input type="text" class="form-control front-no-box-shadow" name="applicationDes" id="label_name1ForTwo"/>
                    </div>
                </div>
                <div class="form-group" style="display: none">
                    <div class="col-lg-7">
                        <label>标签描述</label>
                        <input type="text" class="form-control front-no-box-shadow" name="applicationDes" id="label_info1ForTwo"/>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="btn_submit1ForTwo" class="btn btn-primary" data-dismiss="modal"
                        onclick="updateLabelForTwo()"><a>确定</a></button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="addSameName" tabindex="-1" role="dialog" aria-labelledby="addMemberLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="same_header">无法执行此操作</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="col-lg-7">
                        <label>该分类名称已存在！</label>
                        <input type="text" class="form-control front-no-box-shadow" name="applicationDes" id="tagIdSame" style="display:none;"/>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="cancelSame" class="btn btn-primary" data-dismiss="modal"
                        onclick="hideAddSameName()"><a>确定</a></button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="delMemberWrong" tabindex="-1" role="dialog" aria-labelledby="addMemberLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="wrong_header">无法执行此操作</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="col-lg-7">
                        <label>请先删除该分类下的二级分类！</label>
                        <input type="text" class="form-control front-no-box-shadow" name="applicationDes" id="tagIdWrong" style="display:none;"/>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="cancelWrong" class="btn btn-primary" data-dismiss="modal"
                        onclick="hideDelDialogWrong()"><a>确定</a></button>
            </div>
        </div>
    </div>
</div>
</body>

<!-- START: Scripts -->
<c:import url="../template/_include_js.jsp"/>
<script type="text/javascript">
    let state = 0;
    mount();
    function mount() {
        state = 0;
        $.ajax({
            type: "get",
            url: "/labels",
            contentType: "application/json;charset=utf-8",
            success: function (data) {
                if (data.code === 200) {
                    labelList = data.result;
                    // console.log(labelList)
                    load(labelList);
                } else
                    message(data.msg, false);
            }, error: function (err) {
                message("添加失败：" + err, false);
            }
        });
    }
    document.addEventListener("keyup",function(e){
        if(e.keyCode == 13){

            if(state == 1){
                addLabelForTwo();
            }
            if(state == 2){
                updateLabelForTwo();
            }
            if(state == 3){
                delLabelForTwo();
            }
            if(state == 4){
                addLabel();
            }
            if(state == 5){
                updateLabel();
            }
            if(state == 6){
                delLabel();
            }
        }
    });

    function load(tagList){
        var htmlStr = "";
        for(var j = 0; j < tagList.length; j++){
            htmlStr += "<div class=\"col-md-12 row\" style='margin-left: 35px;padding-bottom: 0px;'>";
            for(var i = 0 ;i < tagList[j].length; i++) {
                switch (i) {
                    case 0: {
                        var tagId = tagList[j][i].id;
                        var useUpId = tagList[j][0].id;
                        var labelName = tagList[j][i].name;
                        var labelInfo = tagList[j][i].info;
                        htmlStr += "<div class=\"dropdown\">";
                        htmlStr += "<button data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\" class=\"btn btn-secondary testclassword btn-outline-light dropdown-toggle re-btn\" style=\"background-color: rgba(247, 247, 247, 0.8); color:black; font-size:1rem; margin-right: 5px;\">" + tagList[j][i].name + "</button>";
                        htmlStr += "<div class=\"dropdown-menu\">"
                        htmlStr += "<button class=\"dropdown-item\" onclick=\"showUpdateDialog("+tagId+","+useUpId+","+"\'"+labelName+"\'"+","+"\'"+labelInfo+"\'"+")\">修改该类别</button>"
                        htmlStr += "<button class=\"dropdown-item\" onclick=\"showDelDialog(" + tagId + ")\">删除该类别</button>"
                        htmlStr += "</div>"
                        htmlStr += "</div>";
                        htmlStr += "<div class=\"col-md-10\">";
                        break;
                    }
                    default: {
                        var tagId = tagList[j][i].id;
                        var useUpId = tagList[j][0].id;
                        var useUpName = tagList[j][0].name;
                        var labelName = tagList[j][i].name;
                        var labelInfo = tagList[j][i].info;
                        htmlStr += "<div class='float-left' style=\"margin-bottom: 10px\">";
                        htmlStr += "<button id="+tagId+ "di" +" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\" class=\"btn btn-secondary btn-outline-light dropdown-toggle re-btn\" style=\"background-color: rgba(247, 247, 247, 0.8); color:rgba(122, 122, 122, 0.8); font-size:1rem; margin-right: 5px;\">" + tagList[j][i].name + "</button>";
                        htmlStr += "<div id="+tagId + "div" +" class=\"dropdown-menu\">"
                        htmlStr += "<button id="+tagId+ "b2" +" class=\"dropdown-item\" onclick=\"showUpdateDialogForTwo("+tagId+","+useUpId+","+"\'"+labelName+"\'"+","+"\'"+labelInfo+"\'"+","+"\'"+useUpName+"\'"+")\">修改该类别</button>"
                        htmlStr += "<button id="+tagId+ "b3" +" class=\"dropdown-item\" onclick=\"showDelDialogForTwo(" + tagId + ")\">删除该类别</button>"
                        htmlStr += "</div>"
                        htmlStr += "</div>"
                    }
                }
            }
            var useUpId = tagList[j][0].id;
            var useNameForTwo = tagList[j][0].name;
            htmlStr += "<div class=\"col-md-2 float-left\">";
            htmlStr += "<button class=\"re-btn\" style=\"background-color: #007bff; color:white; font-size:1rem; margin-right: 5px;\" onclick=\"showAddDialogForTwo("+useUpId+","+"\'"+useNameForTwo+"\'"+")\">新建</button>";
            htmlStr += "</div>";
            htmlStr += "</div>";
            htmlStr += " <br/>";
            htmlStr += "</div>";
        }
        htmlStr += "<div class=\"col-md-12 row\" style='padding-bottom: 10px;'>";
        htmlStr += "<div class=\"col-md-2 testclass\">";
        htmlStr += "<button class=\"re-btn\" style=\"background-color: #007bff; color:white; font-size:1rem; margin-left: 35px;\" onclick=\"showAddDialog("+0+")\">新建</button>";
        htmlStr += "</div>";
        htmlStr += "</div>";
        $("#labels").html(htmlStr);
    }

    function showAddDialog(useUpId){
        state = 4;
        $('#label_name').val("");
        $('#addMember').modal();
        $('#label_uplevelId').val(useUpId);
    }
    function showAddDialogForTwo(useUpId,useNameForTwo){
        state = 1;
        $('#label_nameForTwo').val("");
        $('#addMemberForTwo').modal();
        $('#label_uplevelIdForTwo').val(useUpId);
        $('#label_uplevelNameForTwo').val(useNameForTwo);
    }

    function showDelDialog(tagId){
        state = 6;
        var num = 0;
        var allLabel;
        $.ajax({
            type: "get",
            url: "/labels",
            contentType: "application/json;charset=utf-8",
            success: function (data) {
                if (data.code === 200) {
                    allLabel = data.result;
                    for(var j = 0; j < allLabel.length; j++){
                        for(var i = 0 ;i < allLabel[j].length; i++){
                            if(allLabel[j][i].uplevelid == tagId){
                                num = 1;
                            }
                        }
                    }
                    if(num == 0) {
                        $('#delMember').modal();
                        $('#tagId').val(tagId);
                    }else{
                        $('#delMemberWrong').modal();
                    }
                } else
                    message(data.msg, false);
            }, error: function (err) {
                message("添加失败：" + err, false);
            }
        });
    }
    function showDelDialogForTwo(tagId){
        state = 3;
        $('#delMemberForTwo').modal();
        $('#tagIdForTwo').val(tagId);
    }
    function hideDelDialog(){
        $('#delMember').hide();
    }
    function hideDelDialogForTwo(){
        $('#delMemberForTwo').hide();
    }

    function showUpdateDialog(tagIdUpdate,useUpIdUpdate,labelNameUpdate,labelInfoUpdate){
        state = 5;
        $('#updateMember').modal();
        $('#label_uplevelId1').val(useUpIdUpdate);
        $('#tagId1').val(tagIdUpdate);
        $('#label_name1').val(labelNameUpdate);
        $('#label_info1').val(labelInfoUpdate);
    }
    function showUpdateDialogForTwo(tagIdUpdate,useUpIdUpdate,labelNameUpdate,labelInfoUpdate,useUpName){
        state = 2;
        $('#updateMemberForTwo').modal();
        $('#label_uplevelId1ForTwo').val(useUpIdUpdate);
        $('#tagId1ForTwo').val(tagIdUpdate);
        $('#label_name1ForTwo').val(labelNameUpdate);
        $('#label_info1ForTwo').val(labelInfoUpdate);
        $('#label_uplevelName1ForTwo').val(useUpName);
    }

    function hideDelDialogWrong(){
        $('#delMemberWrong').hide();
    }
    function hideAddSameName(){
        $('#addSameName').hide();
    }

    function addLabel() {
        $('#addMember').modal("hide");
        let num = 0;
        const name = $("#label_name").val();
        const info = 123;
        const uplevelId = $("#label_uplevelId").val();
        $('#label_name').val("");

        const newLabel = {
            labelName: name,
            labelInfo: info,
            uplevelId: uplevelId
        };
        $.ajax({
            type: "get",
            url: "/labels",
            contentType: "application/json;charset=utf-8",
            success: function (data) {
                if (data.code === 200) {
                    allLabel = data.result;
                    for(var j = 0; j < allLabel.length; j++){
                        for(var i = 0 ;i < allLabel[j].length; i++){
                            if(allLabel[j][i].name == name){
                                num = 1;
                            }
                        }
                    }
                    if(num == 0) {
                        $.ajax({
                            type: "post",
                            url: "/labels/add",
                            data: newLabel,
                            success: function (jsonObject) {
                                if (jsonObject.code === 200) {
                                    mount();
                                }
                            },
                        });
                    }else{
                        $('#addSameName').modal();
                    }
                } else
                    message(data.msg, false);
            }, error: function (err) {
                message("添加失败：" + err, false);
            }
        });
    }
    function delLabel() {
        $('#delMember').modal("hide");
        const id = $("#tagId").val();
        $.ajax({
            type: "delete",
            url: "/labels/" + id,
            contentType: "application/json",
            success: function (jsonObject) {
                if (jsonObject.code === 200) {
                    console.log("成功")
                    mount();
                } else
                    console.log("失败")
            },
        });
    }
    function updateLabel() {
        $('#updateMember').modal("hide");
        const id = $("#tagId1").val();
        const name = $("#label_name1").val();
        const info = $("#label_info1").val();
        const updateLabel = {
            labelName: name,
            labelInfo: info
        };
        $.ajax({
            type: "patch",
            url: "/labels/" + id,
            data: updateLabel,
            success: function (jsonObject) {
                if (jsonObject.code === 200)
                    mount();
                else
                    console.log("失败")
            },
        });
    }

    function addLabelForTwo() {
        $('#addMemberForTwo').modal("hide");
        var num_for_two = 0;
        const name = $("#label_nameForTwo").val();
        const info = 123;
        const uplevelId = $("#label_uplevelIdForTwo").val();
        $('#label_nameForTwo').val("");

        const newLabel = {
            labelName: name,
            labelInfo: info,
            uplevelId: uplevelId
        };
        $.ajax({
            type: "get",
            url: "/labels",
            contentType: "application/json;charset=utf-8",
            success: function (data) {
                if (data.code === 200) {
                    allLabel = data.result;
                    for(var j = 0; j < allLabel.length; j++){
                        for(var i = 0 ;i < allLabel[j].length; i++){
                            if(allLabel[j][i].name == name){
                                num_for_two = 1;
                            }
                        }
                    }
                    if(num_for_two == 0) {
                        $.ajax({
                            type: "post",
                            url: "/labels/add",
                            data: newLabel,
                            success: function (jsonObject) {
                                if (jsonObject.code === 200) {
                                    mount();
                                } else
                                    console.log("失败")
                            },
                        });
                    }else{
                        $('#addSameName').modal();
                    }
                } else
                    message(data.msg, false);
            }, error: function (err) {
                message("添加失败：" + err, false);
            }
        });
    }
    function delLabelForTwo() {
        $('#delMemberForTwo').modal("hide");
        const id = $("#tagIdForTwo").val();
        $.ajax({
            type: "delete",
            url: "/labels/" + id,
            contentType: "application/json",
            success: function (jsonObject) {
                if (jsonObject.code === 200) {
                    console.log("成功")
                    mount();
                } else
                    console.log("失败")
            },
        });
    }
    function updateLabelForTwo() {
        $('#updateMemberForTwo').modal("hide");
        const id = $("#tagId1ForTwo").val();
        const name = $("#label_name1ForTwo").val();
        const info = $("#label_info1ForTwo").val();
        const updateLabel = {
            labelName: name,
            labelInfo: info
        };
        $.ajax({
            type: "patch",
            url: "/labels/" + id,
            data: updateLabel,
            success: function (jsonObject) {
                if (jsonObject.code === 200)
                    mount();
                else
                    console.log("失败")
            },
        });
    }

</script>
</html>
