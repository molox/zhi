/**
 * Author: ZhaoHaotian.
 * Date: 2021-5-4.
 */
// rely on jQuery
(function () {
    // bind to tinymce
    let global = tinymce.util.Tools.resolve('tinymce.PluginManager');

    // toolbarSticky 启用选项 ( true启用 )
    let my_toolbar_sticky = false;

    // toolbarSticky 常驻选项 ( 总是sticky / 聚焦sticky )
    let toolbar_sticky_always = true;
    let sticky_type_status = 'always';

    // toolbarSticky 元素选项 ( 头部元素 )
    let toolbar_sticky_elem = null;
    let toolbar_sticky_elem_autohide = false;

    // elements
    let elements = {};
    // stickyInfo
    let info = {};

    class ToolbarSticky {

        // constructor
        constructor(editor) {
            // console.log('my_toolbar_sticky working...');

            // get params
            my_toolbar_sticky = editor.getParam('my_toolbar_sticky', false, 'bool');
            toolbar_sticky_always = editor.getParam('toolbar_sticky_always', true, 'bool');
            toolbar_sticky_elem = editor.getParam('toolbar_sticky_elem', '', 'string');
            toolbar_sticky_elem_autohide = editor.getParam('toolbar_sticky_elem_autohide', false, 'bool');

            // do on-off
            if (!my_toolbar_sticky) {
                return;
            }

            // get elements
            this.editor = editor;
            // get elements
            $(document).ready(function () {
                elements.editor = $('.tox-tinymce');
                elements.header = $('.tox-editor-header');
                elements.container = $('.tox-editor-container');
                elements.statusbar = $('.tox-statusbar');
                elements.nav = $(toolbar_sticky_elem);
                if(!toolbar_sticky_elem_autohide){
                    elements.nav.removeClass('dx-navbar-autohide');
                }
            });

            // do sticky always
            if (toolbar_sticky_always) {
                editor.on('focus', () => {
                    sticky_type_status = 'focus';
                    this.sticky();
                });
                editor.on('blur', () => {
                    sticky_type_status = 'blur';
                    this.restore();
                });
            } else {
                sticky_type_status = 'always';
                this.sticky();
            }

            return this;
        }

        // do sticky
        sticky() {
            // 防止全屏冲突
            if (elements.editor.hasClass('tox-fullscreen')) {
                this.restore();
                return !1;
            }

            // 获取位置
            this.position();

            // 吸附判定
            if (info.isSticky) {
                // 吸附 toolbar
                // console.log('is sticky');
                elements.container.css('padding-top', elements.header[0].clientHeight);
                elements.editor.removeClass('tox-tinymce--toolbar-sticky-off').addClass('tox-tinymce--toolbar-sticky-on');
                elements.header.css({
                    position: 'fixed',
                    width: info.width,
                    left: info.left,
                    top: info.elemHeight,
                    borderTop: '1px solid #ccc'
                });
                return !1;
            } else {
                // 恢复 toolbar
                this.restore();
            }
        }

        // do restore
        restore() {
            elements.editor.removeClass('tox-tinymce--toolbar-sticky-on').addClass('tox-tinymce--toolbar-sticky-off');
            elements.container.css('padding-top', 0);
            elements.header.attr('style', '');
        }

        // get position and judge sticky
        position() {
            // 获取参数
            let elemOffsetHeight = info.elemHeight;
            // console.log('offsetHeight:' + elemOffsetHeight);

            info.scrollTop = $(window).scrollTop();
            info.width = elements.container.width();
            info.left = elements.editor.offset().left + 1;
            // console.log('scorlltop:' + info.scrollTop);

            // 判定 isSticky
            let beginSticky = elements.container.offset().top - elemOffsetHeight;
            let endSticky = elements.statusbar.offset().top - elemOffsetHeight - elements.header.height();
            info.isSticky = (info.scrollTop >= beginSticky && info.scrollTop <= endSticky
                && (sticky_type_status === 'focus' || sticky_type_status === 'always'));
            // console.log('isSticky:' + info.isSticky);
            // console.log('begin:' + beginSticky)
            // console.log('end:' + endSticky)
        }

    }

    /*********** Throttle scroll Begin ***********/
    /**
     * Throttle scroll
     * thanks: https://jsfiddle.net/mariusc23/s6mLJ/31/
     */

    let $wnd = $(window);
    let hideOnScrollList = [];
    let didScroll = void 0;
    let lastST = 0;

    $wnd.on('scroll load resize orientationchange', function () {
        if (hideOnScrollList.length) {
            didScroll = true;
        }
    });

    function hasScrolled() {
        let ST = $wnd.scrollTop();
        let docH = $(document).height();
        let wndH = $(window).height();

        var type = ''; // [up, down, end, start]

        if (ST > lastST) {
            type = 'down';
        } else if (ST < lastST) {
            type = 'up';
        } else {
            type = 'none';
        }

        if (ST === 0) {
            type = 'start';
        } else if (ST >= docH - wndH) {
            type = 'end';
        }

        hideOnScrollList.forEach(function (item) {
            if (typeof item === 'function') {
                item(type, ST, lastST, $wnd);
            }
        });

        lastST = ST;
    }

    setInterval(function () {
        if (didScroll) {
            didScroll = false;
            window.requestAnimationFrame(hasScrolled);
        }
    }, 250);

    function throttleScroll(callback) {
        hideOnScrollList.push(callback);
    }

    /*********** Throttle scroll End ***********/

    function Plugin() {
        global.add('toolbarsticky', function (editor) {
            editor.on('init', function (e) {
                let tbs = new ToolbarSticky(editor);
                // 监听滚动事件
                throttleScroll(function (type, scroll) {
                    // get offset height as nav
                    let start = 400;
                    if(toolbar_sticky_elem_autohide){
                        if (type === 'down' && scroll > start) {
                            info.elemHeight = 0;
                        } else if (type === 'up' || type === 'end' || type === 'start') {
                            info.elemHeight = elements.nav.outerHeight();
                        }
                    } else {
                        info.elemHeight = elements.nav.outerHeight();
                    }
                    // go sticky
                    // console.log('go sticky');
                    tbs.sticky();
                })
            });
        });
    }

    Plugin();

}());
