<%--
  Created by IntelliJ IDEA.
  User: HUYUZHU
  Date: 2020/1/9
  Time: 13:20
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<c:choose>
    <%--搜索结果为0时    --%>
    <c:when test="${recordNum==0}">
        <div class="resource-post-box pb-25">
            <span style="color: darkgray" class="font-text">
            没有找到相关资源
            </span>
            <span id="search-criteria" style="float:right;color: darkgray" class="font-text"><a
                    style="text-decoration:none" href="javascript:void(0);"
                    onclick="clickCriteria(1,1)">展开搜索条件</a></span>
        </div>
        <div id="search-criteria-text" class="resource-post-box" style="display: none">
            <c:if test="${!queryStr.equals('') and queryStr!=null}">
                <div class="col-md-12 row mnt-7" style="display:block">
                    <a class="re-btn" disabled="disabled"
                       style="padding-left: 0px; pointer-events: none; background-color:white; color:#1b1b1b; font-weight: 400; font-size:1rem;">搜索</a>
                    <button class="re-btn"
                            style="margin-right:10px;background-color: rgba(247, 247, 247, 0.8);color: #7a7a7a;pointer-events: none;">${queryStr}</button>
                </div>
                <br/>
            </c:if>
            <c:if test="${!searchGroupNames.equals('') and searchGroupNames!=null}">
                <div class="col-md-12 row" style="margin-top: -10px;display:block">
                    <a class="re-btn" disabled="disabled"
                       style="padding-left: 0px; pointer-events: none; background-color:white; color:#1b1b1b; font-weight: 400; font-size:1rem;">群组</a>
                    <c:forEach var="searchGroupNames" items="${searchGroupNames.split(',')}">
                        <button class="re-btn"
                                style="margin-right:5px;background-color: rgba(247, 247, 247, 0.8);color: #7a7a7a;pointer-events: none;">${searchGroupNames.trim()}</button>
                    </c:forEach>
                </div>
                <br/>
            </c:if>
            <c:if test="${!searchLabelInfos.equals('') and searchLabelInfos!=null}">
                <c:forEach items="${searchLabelInfos}" var="labelInfoList" varStatus="labelInfoListStatus" >
                    <div class="col-md-12 row" style="margin-top: -10px;display:block">
                        <a class="re-btn" disabled="disabled"
                           style="padding-left: 0px; pointer-events: none; background-color:white; color:#1b1b1b; font-weight: 400; font-size:1rem;">
                                ${labelInfoList.get(0).get("labelName")}
                        </a>
                        <c:forEach items="${labelInfoList}" var="labelInfo" varStatus="labelInfoStatu" begin="1">
                            <button class="re-btn"
                                    style="margin-right:5px;background-color: rgba(247, 247, 247, 0.8);color: #7a7a7a;pointer-events: none;">${labelInfo.get("labelName")}</button>
                        </c:forEach>
                    </div>
                    <br/>
                </c:forEach>
            </c:if>
            <div class="col-md-12 row" style="margin-top: -10px;">
                <button class="re-btn" type="submit" onclick="searchAgain('${queryStr}','${groupIds}','${labelIds}')"><a>重新搜索</a>
                </button>
            </div>
        </div>
    </c:when>
    <%--有搜索结果时    --%>
    <c:otherwise>
        <div class="resource-post-box pt-30 pb-30" id="firstrows">
            <span style="margin-right:5px;color: darkgray" class="font-text">找到 ${recordNum} 个相关资源</span>
            <span id="search-criteria1" style="float:right;color: darkgray" class="font-text"><a href="javascript:void(0);" onclick="clickCriteria(1,2)">展开搜索条件</a></span>
        </div>
        <div id="search-criteria-text1" class="resource-post-box" style="display: none">
            <c:if test="${!queryStr.equals('') and queryStr!=null}">
                <div class="col-md-12 row" style="margin-top: -10px;display:block">
                    <a class="re-btn" disabled="disabled"
                       style="padding-left: 0px; pointer-events: none; background-color:white; color:#1b1b1b; font-weight: 400; font-size:1rem;">搜索</a>
                    <button class="re-btn"
                            style="margin-right:10px;background-color: rgba(247, 247, 247, 0.8);color: #7a7a7a;pointer-events: none;">${queryStr}</button>
                </div>
                <br/>
            </c:if>
            <c:if test="${!searchGroupNames.equals('') and searchGroupNames!=null}">
                <div class="col-md-12 row" style="margin-top: -10px;display:block">
                    <a class="re-btn" disabled="disabled"
                       style="padding-left: 0px; pointer-events: none; background-color:white; color:#1b1b1b; font-weight: 400; font-size:1rem;">群组</a>
                    <c:forEach var="searchGroupNames" items="${searchGroupNames.split(',')}">
                        <button class="re-btn"
                                style="margin-right:5px;background-color: rgba(247, 247, 247, 0.8);color: #7a7a7a;pointer-events: none;">${searchGroupNames.trim()}</button>
                    </c:forEach>
                </div>
                <br/>
            </c:if>
            <c:if test="${!searchLabelInfos.equals('') and searchLabelInfos!=null}">
                <c:forEach items="${searchLabelInfos}" var="labelInfoList" varStatus="labelInfoListStatus" >
                    <div class="col-md-12 row" style="margin-top: -10px;display:block">
                        <a class="re-btn" disabled="disabled"
                           style="padding-left: 0px; pointer-events: none; background-color:white; color:#1b1b1b; font-weight: 400; font-size:1rem;">
                                ${labelInfoList.get(0).get("labelName")}
                        </a>
                        <c:forEach items="${labelInfoList}" var="labelInfo" varStatus="labelInfoStatu" begin="1">
                            <button class="re-btn"
                                    style="margin-right:5px;background-color: rgba(247, 247, 247, 0.8);color: #7a7a7a;pointer-events: none;">${labelInfo.get("labelName")}</button>
                        </c:forEach>
                    </div>
                    <br/>
                </c:forEach>
            </c:if>
            <div class="col-md-12 row" style="margin-top: -10px;">
                <button class="re-btn" type="submit" onclick="searchAgain('${queryStr}','${groupIds}','${labelIds}')"><a>重新搜索</a>
                </button>
            </div>
        </div>
        <div class="re-resource-table re-resource-table-default">
            <table class="re-resource-table re-resource-table-default">
                <tbody id="result-column">
                <c:forEach var="app" items="${resourceESList}">
                    <tr>
                        <c:if test="${app.text == null}">
                            <th scope="row" style="padding-right: 50px" class="re-resource-table-topics">
                                <c:if test="${app.attach_search_flag == 0 and app.comment_search_flag == 0}">
                                    <span style="margin-right: 10px"><a class="font-2" href="<%=request.getContextPath()%>/resource/resource?id=${app.id}" target="_blank">${app.title}</a></span>
                                </c:if>
                                <c:if test="${app.attach_search_flag == 1 and app.comment_search_flag == 0}">
                                    <span style="margin-right: 10px"><a class="font-2" href="<%=request.getContextPath()%>/resource/resource?id=${app.id}" target="_blank">${app.title}</a>&nbsp;&nbsp;<span class="iconfont icon-fujian"></span></span>
                                </c:if>
                                <c:if test="${app.attach_search_flag == 0 and app.comment_search_flag == 1}">
                                    <span style="margin-right: 10px"><a class="font-2" href="<%=request.getContextPath()%>/resource/resource?id=${app.id}" target="_blank">${app.title}</a>&nbsp;&nbsp;<span class="iconfont icon-pinglunxiao"></span></span>
                                </c:if>
                                <c:if test="${app.attach_search_flag == 1 and app.comment_search_flag == 1}">
                                    <span style="margin-right: 10px"><a class="font-2" href="<%=request.getContextPath()%>/resource/resource?id=${app.id}" target="_blank">${app.title}</a>&nbsp;&nbsp;<span class="iconfont icon-fujian"></span>&nbsp;&nbsp;<span class="iconfont icon-pinglunxiao"></span></span>
                                </c:if>
<%--                                <span style="margin-right: 10px"><a class="font-2" href="<%=request.getContextPath()%>/resource/resource?id=${app.id}" target="_blank">${app.title}</a></span>--%>
                                <c:if test="${!app.label_name.equals(\"该资源无标签\")}">
                                <span style="float:right">
                                    <c:forEach var="label" items="${app.label_name.split(',|，')}" varStatus="s">
                                        <c:forEach var="labelId" items="${app.label_id.split(',|，')}" varStatus="gi">
                                            <c:if test="${s.index==gi.index}">
                                                <c:choose>
                                                    <c:when test="${s.last}">
                                                        <span class="font-text-darkgray"><a class="link-darkgray" href="../labels/labels?id=${labelId.trim()}">${label.trim()}</a>&nbsp;</span>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <span class="font-text-darkgray"><a class="link-darkgray" href="../labels/labels?id=${labelId.trim()}">${label.trim()}</a>&nbsp;&nbsp;/&nbsp;&nbsp;</span>
                                                    </c:otherwise>
                                                </c:choose>
                                            </c:if>
                                        </c:forEach>
                                    </c:forEach>
                                </span>
                                </c:if>
                                <br>
                                <span class="font-text" style="color: darkgray"><a href="/myresource?userId=${app.user_id}" style="color: darkgray">${app.user_name}</a></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <span class="font-text" style="color: darkgray">
                                <fmt:formatDate value="${app.create_time}" pattern="yyyy-MM-dd HH:mm"/>
                                </span>
                                <c:if test="${!app.group_name.equals(\"该资源无群组\")}">
                                    <span style="float:right">
                                        <c:forEach var="groupName" items="${app.group_name.split(',|，')}" varStatus="gn">
                                            <c:forEach var="groupId" items="${app.group_id.split(',|，')}" varStatus="gi">
                                                <c:if test="${gn.index==gi.index}">
                                                    <c:choose>
                                                        <c:when test="${gn.last}">
                                                            <span style="color: darkgray" class="font-text"><a class="link-darkgray" href="../groupinfo?groupid=${groupId.trim()}">${groupName.trim()}</a>&nbsp;</span>
                                                        </c:when>
                                                        <c:otherwise>
                                                            <span style="color: darkgray" class="font-text"><a class="link-darkgray" href="../groupinfo?groupid=${groupId.trim()}">${groupName.trim()}</a>&nbsp;&nbsp;/&nbsp;&nbsp;</span>
                                                        </c:otherwise>
                                                    </c:choose>
                                                </c:if>
                                            </c:forEach>
                                        </c:forEach>
                                    </span>
                                </c:if>
                            </th>
                        </c:if>
                        <c:if test="${app.text != null}">
                            <th scope="row" style="padding-right: 50px" class="re-resource-table-topics">
                                <c:if test="${app.attach_search_flag == 0 and app.comment_search_flag == 0}">
                                    <span style="margin-right: 10px"><a class="font-2" href="<%=request.getContextPath()%>/resource/resource?id=${app.id}" target="_blank">${app.title}</a></span>
                                </c:if>
                                <c:if test="${app.attach_search_flag == 1 and app.comment_search_flag == 0}">
                                    <span style="margin-right: 10px"><a class="font-2" href="<%=request.getContextPath()%>/resource/resource?id=${app.id}" target="_blank">${app.title}</a>&nbsp;&nbsp;<span class="iconfont icon-fujian"></span></span>
                                </c:if>
                                <c:if test="${app.attach_search_flag == 0 and app.comment_search_flag == 1}">
                                    <span style="margin-right: 10px"><a class="font-2" href="<%=request.getContextPath()%>/resource/resource?id=${app.id}" target="_blank">${app.title}</a>&nbsp;&nbsp;<span class="iconfont icon-pinglunxiao"></span></span>
                                </c:if>
                                <c:if test="${app.attach_search_flag == 1 and app.comment_search_flag == 1}">
                                    <span style="margin-right: 10px"><a class="font-2" href="<%=request.getContextPath()%>/resource/resource?id=${app.id}" target="_blank">${app.title}</a>&nbsp;&nbsp;<span class="iconfont icon-fujian"></span>&nbsp;&nbsp;<span class="iconfont icon-pinglunxiao"></span></span>
                                </c:if>
<%--                                <span style="margin-right: 10px"><a class="font-2" href="<%=request.getContextPath()%>/resource/resource?id=${app.id}" target="_blank">${app.title}</a></span>--%>
                                <c:if test="${!app.label_name.equals(\"该资源无标签\")}">
                                    <span style="float:right">
                                        <c:forEach var="label" items="${app.label_name.split(',|，')}" varStatus="s">
                                            <c:forEach var="labelId" items="${app.label_id.split(',|，')}" varStatus="gi">
                                                <c:if test="${s.index==gi.index}">
                                                    <c:choose>
                                                        <c:when test="${s.last}">
                                                            <span class="font-text-darkgray"><a class="link-darkgray" href="../labels/labels?id=${labelId.trim()}">${label.trim()}</a>&nbsp;</span>
                                                        </c:when>
                                                        <c:otherwise>
                                                            <span class="font-text-darkgray"><a class="link-darkgray" href="../labels/labels?id=${labelId.trim()}">${label.trim()}</a>&nbsp;&nbsp;/&nbsp;&nbsp;</span>
                                                        </c:otherwise>
                                                    </c:choose>
                                                </c:if>
                                            </c:forEach>
                                        </c:forEach>
                                    </span>
                                </c:if>
                                <br>
                                <c:if test="${app.text.length() > 150}">
                                    <span class="font-text"
                                          style="word-break: break-all">${app.text.substring(0,150)}……</span>
                                </c:if>
                                <c:if test="${app.text.length() <= 150}">
                                    <span class="font-text" style="word-break: break-all">${app.text}</span>
                                </c:if>
                                <c:if test="${app.text != null || app.userId != null}">
                                    <br>
                                </c:if>
                                <span class="font-text" style="color: darkgray"><a href="/myresource?userId=${app.user_id}" style="color: darkgray">${app.user_name}</a></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <span class="font-text" style="color: darkgray">
                                    <fmt:formatDate value="${app.create_time}" pattern="yyyy-MM-dd HH:mm"/>
                                </span>
                                <c:if test="${!app.group_name.equals(\"该资源无群组\")}">
                                    <span style="float:right">
                                        <c:forEach var="groupName" items="${app.group_name.split(',|，')}" varStatus="gn">
                                            <c:forEach var="groupId" items="${app.group_id.split(',|，')}" varStatus="gi">
                                                <c:if test="${gn.index==gi.index}">
                                                    <c:choose>
                                                        <c:when test="${gn.last}">
                                                            <span style="color: darkgray" class="font-text"><a class="link-darkgray" href="../groupinfo?groupid=${groupId.trim()}">${groupName.trim()}</a>&nbsp;</span>
                                                        </c:when>
                                                        <c:otherwise>
                                                            <span style="color: darkgray" class="font-text"><a class="link-darkgray" href="../groupinfo?groupid=${groupId.trim()}">${groupName.trim()}</a>&nbsp;&nbsp;/&nbsp;&nbsp;</span>
                                                        </c:otherwise>
                                                    </c:choose>
                                                </c:if>
                                            </c:forEach>
                                        </c:forEach>
                                    </span>
                                </c:if>
                            </th>
                        </c:if>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </c:otherwise>
</c:choose>
<input id="pageSize" type="hidden" value="${pageSize}"/>
<input id="appNum" type="hidden" value="${recordNum}"/>
<style>@font-face {
    font-family:'Glyphicons Halflings';src:url(https://cdn.staticfile.org/twitter-bootstrap/3.3.7/fonts/glyphicons-halflings-regular.eot);src:url(//cdn.staticfile.org/twitter-bootstrap/3.3.7/fonts/glyphicons-halflings-regular.eot?#iefix) format('embedded-opentype'),url(//cdn.staticfile.org/twitter-bootstrap/3.3.7/fonts/glyphicons-halflings-regular.woff) format('woff'),url(//cdn.staticfile.org/twitter-bootstrap/3.3.7/fonts/glyphicons-halflings-regular.ttf) format('truetype'),url(//cdn.staticfile.org/twitter-bootstrap/3.3.7/fonts/glyphicons-halflings-regular.svg#glyphicons_halflingsregular) format('svg')
}
.glyphicon{position:relative;top:1px;display:inline-block;font-family:'Glyphicons Halflings';font-style:normal;font-weight:400;line-height:1;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale}.glyphicon-asterisk:before{content:"\2a"}.glyphicon-plus:before{content:"\2b"}.glyphicon-euro:before,.glyphicon-eur:before{content:"\20ac"}.glyphicon-minus:before{content:"\2212"}.glyphicon-cloud:before{content:"\2601"}.glyphicon-envelope:before{content:"\2709"}.glyphicon-pencil:before{content:"\270f"}.glyphicon-glass:before{content:"\e001"}.glyphicon-music:before{content:"\e002"}.glyphicon-search:before{content:"\e003"}.glyphicon-heart:before{content:"\e005"}.glyphicon-star:before{content:"\e006"}.glyphicon-star-empty:before{content:"\e007"}.glyphicon-user:before{content:"\e008"}.glyphicon-film:before{content:"\e009"}.glyphicon-th-large:before{content:"\e010"}.glyphicon-th:before{content:"\e011"}.glyphicon-th-list:before{content:"\e012"}.glyphicon-ok:before{content:"\e013"}.glyphicon-remove:before{content:"\e014"}.glyphicon-zoom-in:before{content:"\e015"}.glyphicon-zoom-out:before{content:"\e016"}.glyphicon-off:before{content:"\e017"}.glyphicon-signal:before{content:"\e018"}.glyphicon-cog:before{content:"\e019"}.glyphicon-trash:before{content:"\e020"}.glyphicon-home:before{content:"\e021"}.glyphicon-file:before{content:"\e022"}.glyphicon-time:before{content:"\e023"}.glyphicon-road:before{content:"\e024"}.glyphicon-download-alt:before{content:"\e025"}.glyphicon-download:before{content:"\e026"}.glyphicon-upload:before{content:"\e027"}.glyphicon-inbox:before{content:"\e028"}.glyphicon-play-circle:before{content:"\e029"}.glyphicon-repeat:before{content:"\e030"}.glyphicon-refresh:before{content:"\e031"}.glyphicon-list-alt:before{content:"\e032"}.glyphicon-lock:before{content:"\e033"}.glyphicon-flag:before{content:"\e034"}.glyphicon-headphones:before{content:"\e035"}.glyphicon-volume-off:before{content:"\e036"}.glyphicon-volume-down:before{content:"\e037"}.glyphicon-volume-up:before{content:"\e038"}.glyphicon-qrcode:before{content:"\e039"}.glyphicon-barcode:before{content:"\e040"}.glyphicon-tag:before{content:"\e041"}.glyphicon-tags:before{content:"\e042"}.glyphicon-book:before{content:"\e043"}.glyphicon-bookmark:before{content:"\e044"}.glyphicon-print:before{content:"\e045"}.glyphicon-camera:before{content:"\e046"}.glyphicon-font:before{content:"\e047"}.glyphicon-bold:before{content:"\e048"}.glyphicon-italic:before{content:"\e049"}.glyphicon-text-height:before{content:"\e050"}.glyphicon-text-width:before{content:"\e051"}.glyphicon-align-left:before{content:"\e052"}.glyphicon-align-center:before{content:"\e053"}.glyphicon-align-right:before{content:"\e054"}.glyphicon-align-justify:before{content:"\e055"}.glyphicon-list:before{content:"\e056"}.glyphicon-indent-left:before{content:"\e057"}.glyphicon-indent-right:before{content:"\e058"}.glyphicon-facetime-video:before{content:"\e059"}.glyphicon-picture:before{content:"\e060"}.glyphicon-map-marker:before{content:"\e062"}.glyphicon-adjust:before{content:"\e063"}
.glyphicon-tint:before{content:"\e064"}.glyphicon-edit:before{content:"\e065"}.glyphicon-share:before{content:"\e066"}.glyphicon-check:before{content:"\e067"}.glyphicon-move:before{content:"\e068"}.glyphicon-step-backward:before{content:"\e069"}.glyphicon-fast-backward:before{content:"\e070"}.glyphicon-backward:before{content:"\e071"}.glyphicon-play:before{content:"\e072"}.glyphicon-pause:before{content:"\e073"}.glyphicon-stop:before{content:"\e074"}.glyphicon-forward:before{content:"\e075"}.glyphicon-fast-forward:before{content:"\e076"}.glyphicon-step-forward:before{content:"\e077"}.glyphicon-eject:before{content:"\e078"}.glyphicon-chevron-left:before{content:"\e079"}.glyphicon-chevron-right:before{content:"\e080"}.glyphicon-plus-sign:before{content:"\e081"}.glyphicon-minus-sign:before{content:"\e082"}.glyphicon-remove-sign:before{content:"\e083"}.glyphicon-ok-sign:before{content:"\e084"}.glyphicon-question-sign:before{content:"\e085"}.glyphicon-info-sign:before{content:"\e086"}.glyphicon-screenshot:before{content:"\e087"}.glyphicon-remove-circle:before{content:"\e088"}.glyphicon-ok-circle:before{content:"\e089"}.glyphicon-ban-circle:before{content:"\e090"}.glyphicon-arrow-left:before{content:"\e091"}.glyphicon-arrow-right:before{content:"\e092"}.glyphicon-arrow-up:before{content:"\e093"}.glyphicon-arrow-down:before{content:"\e094"}.glyphicon-share-alt:before{content:"\e095"}.glyphicon-resize-full:before{content:"\e096"}.glyphicon-resize-small:before{content:"\e097"}.glyphicon-exclamation-sign:before{content:"\e101"}.glyphicon-gift:before{content:"\e102"}.glyphicon-leaf:before{content:"\e103"}.glyphicon-fire:before{content:"\e104"}.glyphicon-eye-open:before{content:"\e105"}.glyphicon-eye-close:before{content:"\e106"}.glyphicon-warning-sign:before{content:"\e107"}.glyphicon-plane:before{content:"\e108"}.glyphicon-calendar:before{content:"\e109"}.glyphicon-random:before{content:"\e110"}.glyphicon-comment:before{content:"\e111"}.glyphicon-magnet:before{content:"\e112"}.glyphicon-chevron-up:before{content:"\e113"}.glyphicon-chevron-down:before{content:"\e114"}.glyphicon-retweet:before{content:"\e115"}.glyphicon-shopping-cart:before{content:"\e116"}.glyphicon-folder-close:before{content:"\e117"}.glyphicon-folder-open:before{content:"\e118"}.glyphicon-resize-vertical:before{content:"\e119"}.glyphicon-resize-horizontal:before{content:"\e120"}.glyphicon-hdd:before{content:"\e121"}.glyphicon-bullhorn:before{content:"\e122"}.glyphicon-bell:before{content:"\e123"}.glyphicon-certificate:before{content:"\e124"}.glyphicon-thumbs-up:before{content:"\e125"}.glyphicon-thumbs-down:before{content:"\e126"}.glyphicon-hand-right:before{content:"\e127"}.glyphicon-hand-left:before{content:"\e128"}.glyphicon-hand-up:before{content:"\e129"}.glyphicon-hand-down:before{content:"\e130"}.glyphicon-circle-arrow-right:before{content:"\e131"}.glyphicon-circle-arrow-left:before{content:"\e132"}.glyphicon-circle-arrow-up:before{content:"\e133"}.glyphicon-circle-arrow-down:before{content:"\e134"}
.glyphicon-globe:before{content:"\e135"}.glyphicon-wrench:before{content:"\e136"}.glyphicon-tasks:before{content:"\e137"}.glyphicon-filter:before{content:"\e138"}.glyphicon-briefcase:before{content:"\e139"}.glyphicon-fullscreen:before{content:"\e140"}.glyphicon-dashboard:before{content:"\e141"}.glyphicon-paperclip:before{content:"\e142"}.glyphicon-heart-empty:before{content:"\e143"}.glyphicon-link:before{content:"\e144"}.glyphicon-phone:before{content:"\e145"}.glyphicon-pushpin:before{content:"\e146"}.glyphicon-usd:before{content:"\e148"}.glyphicon-gbp:before{content:"\e149"}.glyphicon-sort:before{content:"\e150"}.glyphicon-sort-by-alphabet:before{content:"\e151"}.glyphicon-sort-by-alphabet-alt:before{content:"\e152"}.glyphicon-sort-by-order:before{content:"\e153"}.glyphicon-sort-by-order-alt:before{content:"\e154"}.glyphicon-sort-by-attributes:before{content:"\e155"}.glyphicon-sort-by-attributes-alt:before{content:"\e156"}.glyphicon-unchecked:before{content:"\e157"}.glyphicon-expand:before{content:"\e158"}.glyphicon-collapse-down:before{content:"\e159"}.glyphicon-collapse-up:before{content:"\e160"}.glyphicon-log-in:before{content:"\e161"}.glyphicon-flash:before{content:"\e162"}.glyphicon-log-out:before{content:"\e163"}.glyphicon-new-window:before{content:"\e164"}.glyphicon-record:before{content:"\e165"}.glyphicon-save:before{content:"\e166"}.glyphicon-open:before{content:"\e167"}.glyphicon-saved:before{content:"\e168"}.glyphicon-import:before{content:"\e169"}.glyphicon-export:before{content:"\e170"}.glyphicon-send:before{content:"\e171"}.glyphicon-floppy-disk:before{content:"\e172"}.glyphicon-floppy-saved:before{content:"\e173"}.glyphicon-floppy-remove:before{content:"\e174"}.glyphicon-floppy-save:before{content:"\e175"}.glyphicon-floppy-open:before{content:"\e176"}.glyphicon-credit-card:before{content:"\e177"}.glyphicon-transfer:before{content:"\e178"}.glyphicon-cutlery:before{content:"\e179"}.glyphicon-header:before{content:"\e180"}.glyphicon-compressed:before{content:"\e181"}.glyphicon-earphone:before{content:"\e182"}.glyphicon-phone-alt:before{content:"\e183"}.glyphicon-tower:before{content:"\e184"}.glyphicon-stats:before{content:"\e185"}.glyphicon-sd-video:before{content:"\e186"}.glyphicon-hd-video:before{content:"\e187"}.glyphicon-subtitles:before{content:"\e188"}.glyphicon-sound-stereo:before{content:"\e189"}.glyphicon-sound-dolby:before{content:"\e190"}.glyphicon-sound-5-1:before{content:"\e191"}.glyphicon-sound-6-1:before{content:"\e192"}.glyphicon-sound-7-1:before{content:"\e193"}.glyphicon-copyright-mark:before{content:"\e194"}.glyphicon-registration-mark:before{content:"\e195"}.glyphicon-cloud-download:before{content:"\e197"}.glyphicon-cloud-upload:before{content:"\e198"}.glyphicon-tree-conifer:before{content:"\e199"}.glyphicon-tree-deciduous:before{content:"\e200"}
.glyphicon {
    position: relative;
    top: 1px;
    display: inline-block;
    font-family: 'Glyphicons Halflings';
    -webkit-font-smoothing: antialiased;
    font-style: normal;
    font-weight: normal;
    line-height: 1;
    -moz-osx-font-smoothing: grayscale;
}
</style>
