<%--
  Created by IntelliJ IDEA.
  User: HUYUZHU
  Date: 2020/9/4
  Time: 15:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <c:import url="../template/_header.jsp"/>
    <title>${queryStr}-搜索-知了[团队知识管理应用]</title>
</head>
<body>
<div class="re-main">
    <c:import url="../template/_navbar.jsp"/>
    <header class="re-header re-box-1">
        <div class="container">
            <div class="home-image home-image-parallax">
                <img src="../assets/images/bg-header-2.png" alt="">
                <div style="background-color: rgba(27, 27, 27, .8);"></div>
            </div>

            <div class="row justify-content-center">
                <div class="col-xl-10 col-lg-10 col-md-12 col-sm-12 col-12">
                    <h1 class="mb-30 text-white text-center">团队知识管理</h1>
                    <div class="row">
                        <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2"></div>
                        <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8">
                            <form id="search-form" class="re-form re-form-group-inputs">
                                <%--输入框--%>
                                <input type="text" style="display: none;"/>
                                <input type="hidden" name="keyword" id="hkeyword"/>
                                <input id="query" type="text" name="queryStr" class="form-control" placeholder="在这里发现你想要的知识"/>
                                <button type="button" class="re-btn re-btn-lg" onclick="searchData(1)"><a>搜&nbsp;&nbsp;索</a></button>
                            </form>
                        </div>
                        <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2"></div>
                        <%--                        <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-2"--%>
                        <%--                             style="display: flex;/*实现垂直居中*/align-items: center;padding-left: 0px;">--%>
                        <%--                            <a href="../adsearch/adsearch" style="color: #ffffff">高级</a>--%>
                        <%--                        </div>--%>
                    </div>
                </div>
            </div>

        </div>
    </header>

    <%--    <div class="re-separator"></div>--%>

    <div class="re-box-6 re-grey-6">
        <div class="container">
            <div class="row vertical-gap md-gap">
                <div class="col-lg-12">
                    <div class="re-box re-box-decorated">
                        <div id="search-data" class="resource-post">
                            <div class="resource-post-box pt-30 pb-25">
                                <div class="front-loading">
                                    <img src="../assets/images/loading.gif"/>
                                </div>
                                <div class="panel-body text-center">正在加载请稍候</div>
                            </div>
                        </div>
                        <div id="page-data" class="text-center"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- START: Footer -->
    <c:import url="../template/_footer.jsp"/>
    <!-- END: Footer -->
</div>
</body>
<!-- START: Scripts -->
<c:import url="/template/_include_js.jsp"/>

<script type="text/javascript">
    var query = "${queryStr}";//搜索输入的关键字
    var groupIds = "${groupIds}";//群组
    var labelIds = "${labelIds}";//分类
    var totalZero = 0;
    var appNum = 0;
    var loadinghtml = "<div class=\"resource-post-box pt-30 pb-30\">" +
        "<div class=\"front-loading\">" +
        "<img src=\"../assets/images/loading.gif\"/>" +
        "</div>" +
        "<div class=\"panel-body text-center\">正在加载请稍候</div>" +
        "</div>"

    //搜索框的普通搜索
    $("#query").keydown(function (event) {
        if (event.keyCode == 13) {
            <%
                session.setAttribute("queryStr", request.getParameter("queryStr"));
                //System.out.println("query: "+request.getParameter("queryStr"));
            %>
            var query = $("#query").val().trim();
            var times = 1;
            window.location.href = "<%=request.getContextPath()%>/search/searchAll?queryStr=" + query + "&groupIds=" + "&labelIds=" + "&times=" + times;
        }
    });

    function searchData() {
        var times = 1;
        // times = searchTimes+1;
        <%
            session.setAttribute("queryStr", request.getParameter("queryStr"));
        %>
        var query = $("#query").val().trim();
        window.location.href = "<%=request.getContextPath()%>/search/searchAll?queryStr=" + query + "&groupIds=" + "&labelIds=" + "&times=" + times;
    }

    //异步获取搜索结果列表
    function getPage(page) {
        $("#page-data").html("");//清除页码，必要！！
        $("#search-data").html(loadinghtml);
        $.post("../search/searchdata", {
            queryStr: query,
            page: page - 1,
            groupIds: groupIds,
            labelIds: labelIds,
            searchTimes: 1
        }, function (data) {

            var parser = new DOMParser();
            var htmlDoc = parser.parseFromString("" + data, "text/html");
            var pageSize = parseInt(htmlDoc.getElementById("pageSize").value);
            appNum = parseInt(htmlDoc.getElementById("appNum").value);
            $("#search-data").html(data);
            //页数大于一，分页
            if (pageSize > 1) {
                $("#page-data").html(getDivPageNumHtml(page, pageSize, "getPage") + "<br /><br /><br /><br />");
            }
            document.body.scrollTop = document.documentElement.scrollTop = 0;
        });
    }

    /*
* 最初加载页面的时候，进行的搜索
* */

    $(document).ready(function () {
        //loading
        getPage(1);
    })
</script>
<!-- END: Scripts -->
</html>

