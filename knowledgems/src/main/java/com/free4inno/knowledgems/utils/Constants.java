package com.free4inno.knowledgems.utils;

/**
 * Author HUYUZHU.
 * Date 2020/9/27 11:33.
 */
public class Constants {
    public static final String USERNAME = "未知"; //用户真实姓名默认值
    public static final String ACCOUNT = "account"; //保存在session中的账户（手机号）
    public static final String ACCOUNT_NAME = "accountName"; //保存在session中的账户名(昵称)
    public static final String ACCOUNT_MAIL = "accountMail"; //保存在session中的用户邮箱
    public static final String USER_ID = "userId"; //保存在session中的用户id
    public static final String USER_NAME = "userName"; //保存在session中的用户真实姓名
    public static final String ROLE_ID = "roleId"; //保存在session中的用户类型
    public static final String COMPLETE_URI = "completeUri"; //保存在session中的被拦截的uri
}
