package com.free4inno.knowledgems.controller;

import com.free4inno.knowledgems.dao.*;
import com.free4inno.knowledgems.domain.*;
import com.free4inno.knowledgems.service.ResourceEsService;
import com.free4inno.knowledgems.service.ResourceService;
import com.free4inno.knowledgems.utils.Constants;
import com.free4inno.knowledgems.utils.ImageUtils;
import com.free4inno.knowledgems.utils.StringUtils;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.IOException;
import java.net.MalformedURLException;
import java.sql.Timestamp;
import java.util.*;
import java.util.concurrent.*;

import static com.free4inno.knowledgems.utils.CompleteUriUtils.getCompleteUri;

/**
 * Author HUYUZHU.
 * Date 2020/9/25 13:12.
 */

@Slf4j
@Controller
@RequestMapping("/resource")
public class ResourceController {

    @Autowired
    private ResourceDao resourceDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private CommentDao commentDao;

    @Autowired
    private ResourceService resourceService;

    @Autowired
    private SpecResourceDao specResourceDao;

    @Autowired
    private ResourceEsService esService;

    @Autowired
    private SystemManageDao systemManageDao;

    @Autowired
    private LabelInfoDao labelInfoDao;

    @Autowired
    private ImageUtils imageUtils;

    @RequestMapping("/resource")
    public String resource(@RequestParam(value = "id", required = true) int id,
                           @RequestParam(value = "isBook", required = false) boolean isBook,
                           HttpSession session, HttpServletRequest request, HttpServletResponse httpServletResponse, Map param) {
        log.info(this.getClass().getName() + "----in----" + "获取资源(resource)" + "----" + session.getAttribute(Constants.USER_ID));
        Resource resource = resourceDao.findAllById(id);
        //取用户名
        User user = new User();
        user.setRealName(Constants.USERNAME);
        user = userDao.findById(resource.getUserId()).orElse(user);
        //取群组名
        resource.setGroupName(resourceService.getGroupName(resource));
        //取标签名
        resource.setLabelName(resourceService.getLabelName(resource));
        //取资源的推荐情况
        resourceService.setSpecResource1(resource);
        //取资源的公告情况
        resourceService.setSpecResource2(resource);
        // （此处的保存是为了保存未同步的资源推荐与公告情况）
        resourceDao.save(resource);

        // get book label id
        String bookLabelId = systemManageDao.findAllByVariable("book_label").get().getValue();
        LabelInfo labelInfo = labelInfoDao.findAllById(Integer.parseInt(bookLabelId));
        String bookLabelName = "未知";
        if (labelInfo != null) {
            bookLabelName = labelInfo.getLabelName();
        }

        //取资源的附件
        String attachment = resourceService.getAttachmentJson(id);
        param.put("attachment", attachment);
        //获取这个资源的uri
        String completeUri = getCompleteUri(request);
        session.setAttribute(Constants.COMPLETE_URI, completeUri);
        //判断是否为公开资源
        Boolean isPublic;
        Boolean isInGroup;
        if (resource.getPermissionId() == 0) {
            /* 非公开资源 */
            isPublic = false;
            log.info(this.getClass().getName() + "----" + "非公开资源" + "----");
            // 判断是否登录
            if (session.getAttribute(Constants.ACCOUNT) == null) {
                log.info(this.getClass().getName() + "----out----" + "非公开资源且未登录，跳转登录页面" + "----" + session.getAttribute(Constants.USER_ID));
                try {
                    httpServletResponse.sendRedirect("/login");
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return "/login/login";
            } else {
                // 判断用户是否在资源群组内
                isInGroup = resourceService.checkUserInResourceGroup(resource, (int)session.getAttribute(Constants.USER_ID));
            }
        } else {
            /* 公开资源 */
            isPublic = true;
            isInGroup = true;
            log.info(this.getClass().getName() + "----" + "公开资源" + "----");
        }

        param.put("resource", resource);
        param.put("user", user);
        param.put("bookLabelName", bookLabelName);

        //判断用户是否有权限打开
        if (!isPublic && !isInGroup) {
            log.info(this.getClass().getName() + "----out----" + "非公开资源，且不在群组内" + "----" + session.getAttribute(Constants.USER_ID));
            return "/resource/rejectResource";
        }
        //判断是否书籍
        if (isBook) {
            log.info(this.getClass().getName() + "----out----" + "打开书籍资源" + "----" + session.getAttribute(Constants.USER_ID));
            return "/resource/bookResource";
        }
        log.info(this.getClass().getName() + "----out----" + "打开普通资源" + "----" + session.getAttribute(Constants.USER_ID));
        return "/resource/resource";
    }

    @ResponseBody
    @PostMapping("/changePermission")
    public String changePermission(@RequestParam("id") int id, HttpSession session) {
        log.info(this.getClass().getName() + "----in----" + "修改资源权限(changePermission)" + "----" + session.getAttribute(Constants.USER_ID));

        String result = "";
        Resource resource = resourceDao.findResourceById(id);
        //反转resource.permissionId的值
        if (resource.getPermissionId() == 0) {
            resource.setPermissionId(1);
            result = "permissionId=1";
        } else {
            resource.setPermissionId(0);
            result = "permissionId=0";
        }
        resourceDao.save(resource);
        esService.updateResourceES(id);

        log.info(this.getClass().getName() + "----out----" + "返回反转后的当前资源权限类型" + "----" + session.getAttribute(Constants.USER_ID));
        return result;
    }

    @RequestMapping("/deleteResource")
    public String deleteResource(@RequestParam("id") int id, HttpSession session) {
        log.info(this.getClass().getName() + "----in----" + "删除资源(deleteResource)" + "----" + session.getAttribute(Constants.USER_ID));
        //删除正文中图片
        Resource resource = resourceDao.findResourceById(id);
        String text = resource.getText();
        List<String> imageSrc = imageUtils.getImgSrc(text);
        String result = "";
        for (String src : imageSrc) {
            result = imageUtils.deleteImage(src);
        }
        //删除resource表里的一条数据
        resourceDao.deleteById(id);
        //删除ES索引中的一条数据
        esService.deleteResourceES(id);
        //删除spec表里的一条数据
        if (specResourceDao.findByResourceId(id) != null) {
            specResourceDao.deleteByResourceId(id);
        }
        //删除评论
        if (commentDao.findByResourceId(id) != null && commentDao.findByResourceId(id).size() != 0) {
            commentDao.deleteAllByResourceId(id);
        }
        log.info(this.getClass().getName() + "----out----" + "删除成功" + "----" + session.getAttribute(Constants.USER_ID));
        return "redirect:/";
    }

    @RequestMapping("/getComment")
    public String getComment(@RequestParam("resourceId") int resourceId,
                             @RequestParam("page") int page,
                             Map param, HttpSession session) {
        log.info(this.getClass().getName() + "----in----" + "获取资源评论(getComment)" + "----" + session.getAttribute(Constants.USER_ID));
        int pageSize = 5;
        Sort sort = Sort.by(Sort.Direction.ASC, "time");
        Pageable pageable = PageRequest.of(page, pageSize, sort);
        Page<Comment> commentPage = commentDao.findAllByResourceId(resourceId, pageable);
        List<Comment> commentList = commentPage.getContent();
        int pageTotal = commentPage.getTotalPages();

        // List<Comment> commentList = commentDao.findByResourceId(resourceId);
        for (Comment comment : commentList) {
            String userName = userDao.findById(comment.getUserId()).get().getRealName();
            comment.setUserName(userName);
            int picID = (int) userName.charAt(1) % 20;
            String picName = picID + ".png";
            comment.setPicName(picName);
        }
        param.put("commentList", commentList);
        param.put("pageTotal", pageTotal);
        param.put("resourceId", resourceId);
        if (session.getAttribute(Constants.USER_ID) == null) {
            param.put("userId", "");
            param.put("userName", "");
            session.setAttribute("login_flag", 0);
        } else {
            param.put("userId", session.getAttribute(Constants.USER_ID));
            param.put("userName", userDao.findById((int) session.getAttribute(Constants.USER_ID)).get().getRealName());
        }
        log.info(this.getClass().getName() + "----out----" + "返回资源评论" + "----" + session.getAttribute(Constants.USER_ID));
        return "resource/_comment";
    }

    @ResponseBody
    @RequestMapping("/newComment")
    public String newComment(@RequestParam("content") String content,
                             @RequestParam("resourceId") int resourceId, HttpSession session, HttpServletRequest request) throws Exception {
        log.info(this.getClass().getName() + "----in----" + "新增评论(newComment)" + "----" + session.getAttribute(Constants.USER_ID));
        String contentHtml = "<p class=\"mb-0\">" + StringUtils.inputStringFormat(content).replaceAll("<br/>", "</p><p class=\"mb-0\">") + "</p>";

        List<String> imageSrc = imageUtils.getImgSrc(content);
        //计算程序运行时长的线程池
        final ExecutorService exec = Executors.newFixedThreadPool(1);
        //下载图片，并把img src替换为本地路径
        for (String src : imageSrc) {
            Callable<String[]> call = new Callable<String[]>() {
                public String[] call() throws Exception {
                    //开始执行下载图片操作
                    //**********重要！！download函数内部下载时有一个5s下载时长限制，如若调整下载等待时间，一定要调函数内部**********
                    String[] resultArray = imageUtils.download(src);
                    //**********重要！！download函数内部下载时有一个5s下载时长限制，如若调整下载等待时间，一定要调函数内部**********
                    return resultArray;
                }
            };
            try {
                //控制程序运行时间
                Future<String[]> future = exec.submit(call);
                //**********重要！！下载图片函数超时时间设为 6 秒，超时则报异常**********
                String[] resultArray = future.get(1000 * 6, TimeUnit.MILLISECONDS);
                if (resultArray[0].substring(0, 1).equals("2")) {
                    String newsrc = resultArray[1];
                    log.debug(this.getClass().getName() + "----" + "newdata()" + "----success----" + "图片链接：" + newsrc + "----" + session.getAttribute(Constants.USER_ID));
                    contentHtml = contentHtml.replace(src, newsrc);
                } else if (resultArray[0].substring(0, 1).equals("l")) {
                    log.debug(this.getClass().getName() + "----" + "newdata()" + "----success----" + "图片下载结果：" + resultArray[1] + "----" + session.getAttribute(Constants.USER_ID));
                } else {
                    log.error(this.getClass().getName() + "----" + "newdata()" + "----failure----" + "图片下载出现问题，提示：" + resultArray[0]
                            + "。下载链接为：" + resultArray[1] + "----" + session.getAttribute(Constants.USER_ID));
                    break;
                }
            } catch (TimeoutException e) {
                e.printStackTrace();
                log.error(this.getClass().getName() + "----" + "newdata()" + "----failure----" + "下载外链图片超时" + "----" + session.getAttribute(Constants.USER_ID));
                break; //下载出问题则不再下载图片，直接返回
            } catch (Exception e) {
                e.printStackTrace();
                log.error(this.getClass().getName() + "----" + "newdata()" + "----failure----" + "下载外链图片时出现未知问题" + "----" + session.getAttribute(Constants.USER_ID));
                break; //下载出问题则不再下载图片，直接返回
            }
        }
        // 关闭线程池
        exec.shutdown();
        Comment comment = new Comment();
        comment.setUserId((int) session.getAttribute(Constants.USER_ID));
        comment.setTime(new Timestamp(new Date().getTime()));
        comment.setContent(contentHtml);
        comment.setResourceId(resourceId);
        commentDao.save(comment);
        String result = "ok";

        Resource resource = resourceDao.findResourceById(resourceId);
        ResourceES resourceES = new ResourceES();
        new Thread(new Runnable() {
            @SneakyThrows
            @Override
            public void run() {
                esService.writeResourceToESHTTP(resource, resourceES);
            }
        }).start();
        log.info(this.getClass().getName() + "----out----" + "评论添加成功" + "----" + session.getAttribute(Constants.USER_ID));
        return result;
    }

    @ResponseBody
    @RequestMapping("/deleteComment")
    public String deleteComment(@RequestParam("commentId") int commentId, HttpSession session) {
        log.info(this.getClass().getName() + "----in----" + "删除资源评论(deleteComment)" + "----" + session.getAttribute(Constants.USER_ID));
        commentDao.deleteById(commentId);
        String result = "ok";
        log.info(this.getClass().getName() + "----out----" + "资源评论删除成功" + "----" + session.getAttribute(Constants.USER_ID));
        return result;
    }
}
