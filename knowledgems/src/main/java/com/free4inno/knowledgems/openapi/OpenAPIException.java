package com.free4inno.knowledgems.openapi;

import lombok.Data;

/**
 * Author HUYUZHU.
 * Date 2021/10/24 18:35.
 * OpenAPI 统一异常类
 */

@Data
public class OpenAPIException extends RuntimeException {

    private Integer code;

    public OpenAPIException(ResultEnum resultEnum) {
        super(resultEnum.getMsg());
        this.code = resultEnum.getCode();
    }

}
