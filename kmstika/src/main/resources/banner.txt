${AnsiColor.BRIGHT_BLUE}
              11001                                                                            1101OOOOOOOOOOOOO1
              0    1                                                                        10OOOOOOOOOOOOOOOOO1
      0000   0100001                                                                      0OO
     0   0001    0101                         00111110000   1001        0001                 001001011111111111111111111
     100001      1100011                      001      000    001      001             1100OOOOOOOOOOOOOOOOOOOOOOOOOO1
        00       01O010001                    001      100     001    001             0OOOOOOOOOOOOOOOOOOOOOOOOOOOOOO1
        100000011 100   0001                  001     1101      000  101            11OOOOOOOOOOOOOOOOOOOOOOOOOOOO11
         100001   001     101                 0000000001         000101             0O1OO1OOOO
        101   0000001      001                001     11001       1001              0OOOOOOO1  1OOOOO11OOOOOOOOOOO1
        101     01   00     00                001       100        00               1OOOOOO1 0OOOOOOOOOOOOOOOOOOO1
        100     01  010010   0                001       100        00                0OOOOO1
         001    00011    00  1                001     10001        00                 0OOOO1  10000000000000001
          001   00     1100011                00000000111          00                  10OOO0  10OOOOOOOOOOOO1
           0001 00000010                                                                  00OO     0OO11OO
              00                                                                             10111

${AnsiColor.BRIGHT_YELLOW}:: Running Spring Boot ${spring-boot.version} :: ${AnsiColor.DEFAULT}